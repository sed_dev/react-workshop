# React Workshop

A simple react app bootstrapped with [Create React App](https://github.com/facebook/create-react-app).


## Prerequisites
1. NodeJS > 9.x
2. Git
 

## Installation
1. Clone the project
2. Run `npm install` inside the project folder to install all dependencies.
3. Run `npm start` to run the project in development mode.
 

